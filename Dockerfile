FROM nginx:1.19-alpine

EXPOSE 80

COPY ./src/* /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]